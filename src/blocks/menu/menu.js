$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('body').toggleClass('body--fixed');
  $('.menu__content').fadeToggle();
});
