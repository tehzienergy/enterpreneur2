var scroll = 0

$(window).on('scroll', function() {
  scroll = $(window).scrollTop();
  if (scroll > 1) {
    $('.header').addClass('header--fixed');
  }
  else {
    $('.header').removeClass('header--fixed');
  }
})
